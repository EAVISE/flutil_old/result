from typing import List
from .block import Block
from os.path import abspath


class Sheet:
    """A result sheet consisting of multiple result blocks."""
    def __init__(self, blocks: List[Block], title: str=None,
                 description: str=None):
        self._title = title
        self._description = description
        self._blocks = blocks
        self._contents = ''

        self._evaluate()

    def _evaluate(self):
        """Evaluate all the blocks in the sheet."""
        [b.evaluate() for b in self._blocks]

    def export(self, filename: str):
        """Export the sheet to the given filename.

        This function will call 4 functions:
        1. `_correct_filename`: to put the given filename in a desired format
        (e.g. with the correct filename extension)
        2. `_preprocess`: before creating contents
        3. `_add_title`: adding the global sheet title to the contents
        (if a sheet title is given)
        4. `_add_description`: adding the global sheet description to the
        contents (if a sheet description is given)
        5. `_add_block`: for each block in `self.blocks` to add it to the
        contents.
        6. `_postprocess`: after contents are created

        :param str filename: The filename of the destination results sheet.
        """
        filename = abspath(self._correct_filename(filename))
        self._preprocess(filename)

        if self._title is not None:
            self._add_title(self._title)
        if self._description is not None:
            self._add_description(self._description)
        for b in self._blocks:
            self._add_block(b)

        self._postprocess(filename)

    def _preprocess(self, filename: str):
        """Some preparatory work before creating contents.

        The default behavior is to do nothing. Children should override this
        function if some set-up need be done.

        :param str filename: The filename of the destination results sheet.
        """
        pass

    def _add_title(self, title: str):
        """Add the global sheet title.

        :param str title: the title to add
        :raises NotImplementedError: if the child class did not provide an
        implemention
        """
        raise NotImplementedError

    def _add_description(self, description: str):
        """Add the global sheet description.

        :param str description: the description to add
        :raises NotImplementedError: if the child class did not provide an
        implemention
        """
        raise NotImplementedError

    def _add_block(self, block: Block):
        """Add a block to the contents.

        :param Block block: the Block to add
        :raises NotImplementedError: if the child class did not provide an
        implemention
        """
        raise NotImplementedError

    def _correct_filename(self, filename: str):
        """Return the corrected filename.

        This could be overridden by the child class to put the given
        filename in a certain format. E.g. append it with the correct
        filename extension.

        By default this function will simply return the given filename, without
        any modifications.

        :param str filename: the filename to correct
        """
        return filename

    def _postprocess(self, filename: str):
        """The necessary work after contents are created.

        The default behavior is to simply write `self._contents` (which is an
        empty string by default) to the given `filename`.

        :param str filename: The filename of the destination results sheet.
        """
        with open(filename, 'w') as f:
            f.write(self._contents)
