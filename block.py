from pandas import DataFrame
from matplotlib.figure import Figure


class Block:
    """A block result shows the result of a small experiment.

    It contains a title, a descriptive text and a figure. The block should
    represent a single idea and is usually part of a larger set of blocks that
    show the results of an entire (large) experiment.
    """
    def __init__(self, df: DataFrame, title: str=None,
                 description: str=None):
        self._fig = None
        self._conclusion = None
        self._description = description
        self._title = title
        self._df = df
        self.evaluate()

    def evaluate(self):
        """Evaluate the block result.

        Use the DataFrame to evaluate the results and give the
        block properties (fig, description, title) their values.
        """
        raise NotImplementedError

    @property
    def df(self) -> DataFrame:
        """The `DataFrame` that is being used to evaluate the results."""
        return self._df

    @property
    def description(self) -> str:
        """A description of the experiment."""
        return self._description

    @description.setter
    def description(self, value):
        self._description = value

    @property
    def fig(self) -> Figure:
        """Matplotlib `Figure` that illustrates the result."""
        return self._fig

    @property
    def title(self) -> str:
        """A title for the experiment."""
        return self._title

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def conclusion(self) -> str:
        """An interpretation of the result."""
        return self._conclusion

    @conclusion.setter
    def conclusion(self, value) -> str:
        self._conclusion = value
