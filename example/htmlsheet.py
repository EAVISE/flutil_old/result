from .mdsheet import MdSheet
import os
import subprocess


class HtmlSheet(MdSheet):
    """A pdf result sheet. You should have [pandoc](https://pandoc.org/)
    installed.
    """
    def _preprocess(self, filename):
        super()._preprocess(filename)
        self._figext = 'svg'

    def _postprocess(self, filename: str):
        super()._postprocess(filename)
        htmlfile = f'{os.path.splitext(filename)[0]}.html'

        # Run pandoc
        completed_process = subprocess.run(['pandoc', filename,
                                            '-o', htmlfile])
        completed_process.check_returncode()

        # Clean up unneeded files
        os.remove(filename)
