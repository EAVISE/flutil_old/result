from .mdsheet import MdSheet
import os
import subprocess


class PdfSheet(MdSheet):
    """A pdf result sheet. You should have [pandoc](https://pandoc.org/)
    installed.
    """
    def _postprocess(self, filename: str):
        super()._postprocess(filename)
        pdffile = f'{os.path.splitext(filename)[0]}.pdf'

        # Run pandoc
        completed_process = subprocess.run(['pandoc', filename,
                                            '-o', pdffile])
        completed_process.check_returncode()

        # Clean up unneeded files
        os.remove(filename)
