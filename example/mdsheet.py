from ..sheet import Sheet
from ..block import Block
from matplotlib.figure import Figure
import re
import os
from os.path import splitext, basename, join, isdir, dirname
import os.path


class MdSheet(Sheet):
    """A result sheet in the MarkDown format."""

    def _preprocess(self, filename):
        self._block_count = 0
        self._filename = filename
        self._contents = ''
        self._figext = 'pdf'

    def _add_block(self, block: Block):
        self._block_count += 1

        if block.title is not None:
            block_title = block.title
        else:
            block_title = f'Experiment {self._block_count}'

        # Add experiment title
        self._contents += f'## {block_title}\n\n'

        # Add experiment description
        if block.description is not None:
            self._contents += f'{block.description}\n\n'

        # Add figure
        self._add_block_fig(block.fig, block_title)

        # Add conclusion
        if block.conclusion is not None:
            self._contents += f'{block.conclusion}\n\n'

    def _add_block_fig(self, fig: Figure, block_title: str):
        fig_subdir = f'{splitext(basename(self._filename))[0]}_figs'
        fig_fulldir = join(dirname(self._filename), fig_subdir)

        if not isdir(fig_fulldir):
            os.mkdir(fig_fulldir)

        fig_filename = (block_title.lower().replace(' ', '_')
                        + f'.{self._figext}')

        fig.savefig(join(fig_fulldir, fig_filename))
        self._contents += (f'![{block_title}]'
                           f'({join(fig_fulldir, fig_filename)} '
                           f'"{block_title}")\n\n')

    def _add_description(self, description: str):
        self._contents += f'{description}\n\n'

    def _add_title(self, title: str):
        self._contents += f'# {title}\n\n'

    def _correct_filename(self, filename: str):
        if not re.match('.*\.md$', filename):
            return f'{filename}.md'
        else:
            return filename
